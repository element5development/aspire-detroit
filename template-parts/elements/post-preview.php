<?php
	$category = get_the_category();
	$firstCategory = $category[0]->cat_name;
?>
<article class="post-preview">
	<h2><?php the_title(); ?></h2>
	<p class="subheading"><?php echo $firstCategory; ?></p>
	<?php if ( get_the_excerpt() ) : ?>
		<div class="entry-content">
			<?php the_excerpt(); ?>
		</div>
	<?php endif; ?>
	<?php if ( $firstCategory == 'Job Opportunity' ) : ?>
		<a href="<?php the_permalink(); ?>" class="button">Apply Now</a>
	<?php elseif ( $firstCategory == 'Curriculum Update' ) : ?>
		<a href="<?php the_permalink(); ?>" class="button">View Update</a>
	<?php elseif ( $firstCategory == 'Homework Documents' ) : ?>
		<a href="<?php the_permalink(); ?>" class="button">Download Documents</a>
	<?php else : ?>
		<a href="<?php the_permalink(); ?>" class="button">View More</a>
	<?php endif; ?>
</article>