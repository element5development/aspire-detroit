<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<div class="primary-navigation">
	<nav>
		<a href="<?php echo get_home_url(); ?>"><svg><use xlink:href="#logo"></use></svg></a>
		<button class="toggle-menu is-grey">Menu</button>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
	</nav>
</div>