<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>
<?php $image = get_field('header_image'); ?>

<header class="post-head" style="background-image: url(<?php echo $image['sizes']['large']; ?>);">
	<h1><?php the_title(); ?></h1>
	<?php if ( is_front_page() ) : ?>
		<div class="intro">
			<?php the_field('intro'); ?>
		</div>
	<?php endif; ?>
</header>