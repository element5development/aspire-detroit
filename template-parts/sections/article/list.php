<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a single styled list

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="major-list <?php the_sub_field('width'); ?> <?php the_sub_field('numbered_or_alphabetical'); ?>">
	<ol>
		<?php while( have_rows('list') ) : the_row(); ?>
			<li>
				<div>
					<?php if ( get_sub_field('title') ) : ?>
						<h3><?php the_sub_field('title'); ?></h3>
					<?php endif; ?>
					<?php if ( get_sub_field('subheading') ) : ?>
						<p class="subheading"><?php the_sub_field('subheading'); ?></p>
					<?php endif; ?>
					<?php if ( get_sub_field('description') ) : ?>
						<?php the_sub_field('description'); ?>
					<?php endif; ?>
				</div>
			</li>
		<?php endwhile; ?>
	</ol>
</section>