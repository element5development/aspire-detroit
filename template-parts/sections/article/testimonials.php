<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a testimonial slider

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="testimonials is-full-width">
<!-- 	<div class="graphic">
		<?php $image = get_sub_field('graphic'); ?>
		<img class="lazyload blur-up" data-expand="250" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['large']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 700w, <?php echo $image['sizes']['large']; ?> 1000w, <?php echo $image['sizes']['xlarge']; ?> 1200w"  alt="<?php echo $image['alt']; ?>">
	</div> -->
	<div class="testimonial-slider">
		<?php while( have_rows('testimonials') ) : the_row(); ?>
			<blockquote class="testimonial">
				<p><?php the_sub_field('quote'); ?></p>
				<?php if ( get_sub_field('quotee') ) : ?>
					<p><b><?php the_sub_field('quotee'); ?></b></p>
				<?php endif; ?>
			</blockquote>
		<?php endwhile; ?>
	</div>
</section>