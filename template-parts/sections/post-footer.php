<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<nav>
		<div>
			<a href="<?php echo get_home_url(); ?>" class="logo"><svg><use xlink:href="#logo"></use></svg></a>
			<a href="tel:+12487373787"><svg><use xlink:href="#phone"></use></svg> (248) 737-3787</a>
			<a href="mailto:anthony@aspiredetroit.org"><svg><use xlink:href="#email"></use></svg> anthony@aspiredetroit.org</a>
			<a href="https://www.facebook.com/ChooseToAspire" target="_blank"><svg><use xlink:href="#facebook"></use></svg> Facebook</a>
			<a href="https://www.instagram.com/aspire_detroit/" target="_blank"><svg><use xlink:href="#instagram"></use></svg> Instagram</a>
			<a href="https://www.linkedin.com/company/aspire-detroit/" target="_blank"><svg><use xlink:href="#linkedin"></use></svg> LinkedIn</a>
		</div>
		<?php wp_nav_menu(array( 'theme_location' => 'primary_navigation' )); ?>
	</nav>
	<div class="copyright">
		<p>Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
		<?php wp_nav_menu(array( 'theme_location' => 'legal_navigation' )); ?>
	</div>
</footer>