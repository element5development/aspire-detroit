<?php 
/*----------------------------------------------------------------*\

	DEFAULT POST ARCHIVE TEMPLATE

\*----------------------------------------------------------------*/
?>

<?php 
	//if user is not logged in then redirect to login page
	if (!is_user_logged_in()) {
		wp_redirect( '/login');
		exit;
	} 
?>

<?php 
	$post_type = get_query_var('post_type'); 
	if ( $post_type == '' ) {
		$post_type = 'post';
	}
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php $image = get_field($post_type.'_image','options'); ?>
<header class="post-head" style="background-image: url(<?php echo $image['sizes']['large']; ?>);">
	<h1><?php the_field($post_type.'_title','options'); ?></h1>
	<?php if ( get_field($post_type.'_intro','options') ) : ?>
		<div class="intro">
			<?php the_field($post_type.'_intro','options'); ?>
		</div>
	<?php endif; ?>
</header>

<main id="main-content">
	<article>
		<?php if (have_posts()) : ?>
			<section class="is-extra-wide cat-filter">
				<h6>View Only</h6>
				<form id="category-select" class="category-select" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
					<?php
					$args = array(
							'show_option_none' => __( 'Select category', 'textdomain' ),
							'show_count'       => 1,
							'orderby'          => 'name',
							'echo'             => 0,
					);
					?>
					<?php $select  = wp_dropdown_categories( $args ); ?>
					<?php $replace = "<select$1 onchange='return this.form.submit()'>"; ?>
					<?php $select  = preg_replace( '#<select([^>]*)>#', $replace, $select ); ?>
					<?php echo $select; ?>
					<noscript>
							<input type="submit" value="View" />
					</noscript>
				</form>
			</section>
			<section class="is-extra-wide post-grid">
				<?php	while ( have_posts() ) : the_post(); ?>
					<?php get_template_part('template-parts/elements/post-preview'); ?>
				<?php endwhile; ?>
			</section>
		<?php else : ?>
			<article>
				<section class="is-narrow">
					<p>Uh Oh. Something is missing. Looks like this page has no content.</p>
				</section>
			</article>
		<?php endif; ?>
		<?php clean_pagination(); ?>
	</article>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>