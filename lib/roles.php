<?php
/*----------------------------------------------------------------*\
	ALLOW ANY ROLE TO VIEW PRIVATE POSTS
\*----------------------------------------------------------------*/
function add_sub_caps() {
	global $wp_roles;
	$role = get_role('subscriber');
	$role->add_cap('read_private_posts');
}
add_action ('admin_init','add_sub_caps');
/*----------------------------------------------------------------*\
	REMOVE 'PRIVATE' FROM TITLES
\*----------------------------------------------------------------*/
add_filter( 'private_title_format', function ( $format ) {
	return '%s';
} );
/*----------------------------------------------------------------*\
	REMOVE WP ADMIN BAR FOR NON-ADMIN USERS
\*----------------------------------------------------------------*/
function remove_admin_bar() {
	if ( !current_user_can('administrator') && !is_admin() ) {
		show_admin_bar(false);
	}
}
add_action('after_setup_theme', 'remove_admin_bar');
/*----------------------------------------------------------------*\
	EDITOR ROLE CAPABILITIES
\*----------------------------------------------------------------*/
function add_editor_power(){
	$role = get_role('editor');
	$role->add_cap('gform_full_access');
	$role->add_cap('edit_theme_options');
	$role->add_cap('edit_users');
	$role->add_cap('list_users');
	$role->add_cap('promote_users');
	$role->add_cap('create_users');
	$role->add_cap('add_users');
	$role->add_cap('delete_users');
	$role->add_cap('manage_options');
}
add_action('admin_init','add_editor_power');