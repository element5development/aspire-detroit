<?php
/*----------------------------------------------------------------*\
	LOGIN REDIRECT FOR USERS
\*----------------------------------------------------------------*/
function my_login_redirect( $url, $request, $user ){
	if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
		if( $user->has_cap('administrator') || $user->has_cap('edit_posts') ) {
			$url = admin_url();
		} else {
			$url = home_url('/member-center/');
		}
	}
	return $url;
}
add_filter('login_redirect', 'my_login_redirect', 10, 3 );

/*----------------------------------------------------------------*\
	GAVITY FORM TAB INDEX FIX
\*----------------------------------------------------------------*/
add_filter("gform_tabindex", create_function("", "return false;"));

/*----------------------------------------------------------------*\
	ENABLE LABEL VISABILITY
\*----------------------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*----------------------------------------------------------------*\
	CHANGE SUBMIT INPUTS TO BUTTONS
\*----------------------------------------------------------------*/
function form_submit_button ( $button, $form ){
  $button = str_replace( 'input', 'button class="button"', $button );
  $button = str_replace( "/", "", $button );
  $button .= "{$form['button']['text']}</button>";
  return $button;
}
add_filter( 'gform_submit_button', 'form_submit_button', 10, 5 );