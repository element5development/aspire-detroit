<?php
/*----------------------------------------------------------------*\
	INITIALIZE MENUS
\*----------------------------------------------------------------*/
function nav_creation() {
	$locations = array(
		'primary_navigation' => __( 'Primary Menu' ),
		'legal_navigation' => __( 'Legal Menu' ),
	);
	register_nav_menus( $locations );
}
add_action( 'init', 'nav_creation' );